MTVAA
-----

A idéia desse projeto é utilizar um raspberry pi para tocar vídeos em um pendrive em modo texto.

Materiais:
* Raspberry Pi
* SD Card
* Fonte de alimentação (microusb 5v)
* Pendrive (com os vídeos que deseja reproduzir)
* Cabo para ligar na televisão/monitor (rca ou hdmi)

Passos

* Instalar e configurar o Raspbian
* Configurar o raspbian para iniciar com o usuário pi locado (para isso usar o comando sudo raspi-config e em Boot, escolher a opção B)
* Instalar o mplayer: sudo apt-get install mplayer
* Criar uma pasta para montar o pendrive (sudo mount /media/usb)
* Modificar o /ect/fstab para montar automaticamente qualquer pendrive (sudo nano /etc/fstab e então inserir a linha:
  /dev/sda1 /media/usb vfat defaultas 0 0)
* Configurar o Raspberry para não desligar o monitor e tocar o vídeo. Para isso é necessário modificar o arquivo .profile, que executa comandos no início do sistema. Execute:
  nano .profile
  E dentro do arquivo insira os seguintes comandos na última linha:
  setterm -blank 0 -powerdown 0
  mplayer /media/usb/* --vo=caca --ao=null -fs

O primeiro comando previne a tela de desligar, e o segundo toca todos os vídeos que existirem dentro do pendrive



Se quiser fazer com quê seja exibido o quê está na webcam, utilizar o seguinte comando (considerando que a webcam esteja em /dev/video0):

dd if=/dev/video0 | mplayer tv://device=/dev/stdin --vo=caca
